#include "result.h"
#include <QTextEdit>
#include <QHBoxLayout>
#include <QProcess>

Result::Result(QWidget *parent) :
QDialog(parent)
{

QHBoxLayout *hbox = new QHBoxLayout(this);
QTextEdit *table = new QTextEdit();
table->setReadOnly(true);
hbox->addWidget(table);

    QProcess prc;
    QString exec = "/usr/bin/sh";
    QStringList params;
    params << "-c" << "/usr/bin/sort -rn -k2 /home/dasha/2048.2.0/bestscore/bestscore.txt | /usr/bin/head -10";
    prc.start(exec, params);
    prc.waitForFinished(); // sets current thread to sleep and waits for prc end
    table->insertPlainText(prc.readAllStandardOutput());
    prc.close();

}

Result::~Result()
{
// delete ui;
}
