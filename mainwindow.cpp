#include "mainwindow.h"
#include "tile.h"
#include "result.h"
#include <QInputDialog>
#include <QMessageBox>
#include "colorwindow.h"

MainWindow::MainWindow(int width, int height, QWidget *parent) : QMainWindow(parent)
{
    this->setFixedSize((width > 100 ? width : 100), (height > 100 ? height : 100));

    QMessageBox::information(this, "Shortcuts", "READ ME!\nGame Shortcuts:\nMove tiles with arrows\nReset game - space\nOpen settings dialog - \"S\"\n Information about author - \"I\"\n"
"Rules - \"R\"\nTable with record - \"T\"\nHelp - \"H\"\nFlush record - \"F\"\nQuit - \"Q\"");

    m_grid = new Grid(this, Pos(4,4));

    QObject::connect(this, SIGNAL(move(Grid::Direction)), m_grid, SLOT(move(Grid::Direction)));
    QObject::connect(this, SIGNAL(restartGrid()), m_grid, SLOT(restart()));
    QObject::connect(this, SIGNAL(settings()), this, SLOT(openSettings()));
    QObject::connect(this, SIGNAL(info()), this, SLOT(outputInfo()));
    QObject::connect(this, SIGNAL(rule()), this, SLOT(outputRule()));
    QObject::connect(this, SIGNAL(quit()), this, SLOT(quit_win()));
    QObject::connect(this, SIGNAL(result()), this, SLOT(outputResult()));
    QObject::connect(this, SIGNAL(flush()), this, SLOT(flush_res()));
    QObject::connect(this, SIGNAL(help()), this, SLOT(help_win()));




    m_grid->initGrid();
}

MainWindow::~MainWindow()
{
    //
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    switch (event->key())
    {
    case Qt::Key_Left:
        emit move(Grid::LEFT);
        break;
    case Qt::Key_Right:
        emit move(Grid::RIGHT);
        break;
    case Qt::Key_Up:
        emit move(Grid::UP);
        break;
    case Qt::Key_Down:
        emit move(Grid::DOWN);
        break;
	case Qt::Key_Space:
		emit restartGrid();
		break;
    case Qt::Key_S:
        emit settings();
        break;
    case Qt::Key_Q:
        emit quit();
        break;
    case Qt::Key_I:
        emit info();
        break;
    case Qt::Key_R:
        emit rule();
        break;
    case Qt::Key_T:
        emit result();
        break;
    case Qt::Key_F:
        emit flush();
        break;
    case Qt::Key_H:
        emit help();
        break;
    default:
        break;
    }
}

void MainWindow::openSettings()
{
    ColorWindow* colorWindow = new ColorWindow(this);
    QObject::connect(colorWindow, SIGNAL(newGrid(int,int)), this, SLOT(newGrid(int,int)));
    colorWindow->exec();
}

void MainWindow::newGrid(int rowNumber, int colNumber)
{
    delete m_grid;
    m_grid = new Grid(this, Pos(rowNumber, colNumber));

    QObject::connect(this, SIGNAL(move(Grid::Direction)), m_grid, SLOT(move(Grid::Direction)));
    QObject::connect(this, SIGNAL(restartGrid()), m_grid, SLOT(restart()));

    m_grid->initGrid();
}

void MainWindow::outputInfo()
{
    QMessageBox:: information(this, "About", "Курсовую работу выполнила: Иванченко Дарья Владимировна. Группа: М8О-110Б-20.");
}

void MainWindow::outputRule()
{
    QMessageBox:: information(this, "Rules", "2048 — это игра, где отлично воссоединились традиционный тетрис и пятнашки. "
"Стандартное игровое поле имеет форму квадрата 4x4. Вы можете выбрать размер поля (по умолчанию стоит 4х4). "
"В начале игры вам выдается кирпичик с цифрой «2» или «4», нажимая кнопку вверх, вправо, влево или вниз, все ваши кирпичики будут смещаться в ту же сторону. "
"Но, при соприкосновении клеточек с одним и тем же номиналом, они объединяются и создают сумму вдвое больше. "
"Игра заканчивается тогда, когда все пустые ячейки заполнены, и вы больше не можете передвигать клеточки ни в одну из сторон или когда на одном из кубов, появилась заветная цифра 2048. "
"Удачи!");
}

void MainWindow::outputResult()
{
    Result table;
    table.show();
    table.exec();
}

void MainWindow::quit_win()
{
    QTextStream out(stdout);

      // Создаем объект класса QFile и связываем его с указанным именем файла
      QString filename = "/home/dasha/2048.2.0/bestscore/bestscore.txt";
      QFile file(filename);

      // Открываем файл в режиме "Только для записи"
      if (file.open(QIODevice::Append)) {
        QTextStream out(&file); // поток записываемых данных направляем в файл

        // Для записи данных в файл используем оператор <<
        out << m_grid->getScore() << Qt::endl;
      } else {

        qWarning("Could not open file");
      }
      // Закрываем файл
      file.close();
      QApplication::quit();
}

void MainWindow::flush_res()
{
    QString filename = "/home/dasha/2048.2.0/bestscore/bestscore.txt";
    QFile file(filename);

    // Открываем файл в режиме "Только для записи"
    file.open(QIODevice::WriteOnly);
    // Закрываем файл
    file.close();
}

void MainWindow::help_win()
{
    QMessageBox::information(this, "Shortcuts", "READ ME!\nGame Shortcuts:\nMove tiles with arrows\nReset game - space\nOpen settings dialog - \"S\"\n Information about author - \"I\"\n"
"Rules - \"R\"\nTable with result - \"T\"\nQuit - \"Q\"");
}
