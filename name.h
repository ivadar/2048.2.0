#ifndef NAME_H
#define NAME_H

#include <QDialog>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QFile>
#include <QTextStream>

namespace Ui {
class Name;
}

class Name : public QDialog
{
    Q_OBJECT

public:
    explicit Name(QWidget *parent = nullptr, int m_score = 0);
    ~Name();

private:
    Ui::Name *ui;
};

#endif // NAME_H
