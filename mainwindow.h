#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QApplication>
#include <QWidget>
#include <QKeyEvent>
#include <QPushButton>
#include "grid.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = nullptr);
    explicit MainWindow(int width, int height, QWidget *parent = nullptr);
    ~MainWindow();
    void keyPressEvent(QKeyEvent *event);

public slots:
    void openSettings();
    void outputResult();
    void newGrid(int rowNumber, int colNumber);
    void outputInfo();
    void outputRule();
    void quit_win();
    void flush_res();
    void help_win();

signals:
    void move(Grid::Direction);
    void restartGrid();
    void settings();
    void info();
    void rule();
    void quit();
    void result();
    void flush();
    void help();

private:
    Grid* m_grid;
};

#endif // MAINWINDOW_H
