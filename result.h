#ifndef RESULT_H
#define RESULT_H

#include <QDialog>
#include <QFile>
#include <QTextStream>

namespace Ui {
class Result;
}

class Result : public QDialog
{
    Q_OBJECT

public:
    explicit Result(QWidget *parent = nullptr);
    ~Result();

private:
    Ui::Result *ui;
};

#endif // RESULT_H
