#include "name.h"
#include "grid.h"

Name::Name(QWidget *parent, int m_score) :
    QDialog(parent)
{
    QString name_to_file;

    QLabel *name = new QLabel("Name:", this);
    name->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    QLineEdit *le1 = new QLineEdit(this);
    QGridLayout *grid = new QGridLayout();

      grid->addWidget(name, 0, 0);
      grid->addWidget(le1, 0, 1);

      QString file_with_name = "/home/dasha/2048_git/data/bestscore.txt";
      QFile file(file_with_name);
      if (file.open(QIODevice::WriteOnly)) {
          QTextStream out(&file); // поток записываемых данных направляем в файл
          // Для записи данных в файл используем оператор <<

              name_to_file = name->text();
              out << name_to_file << ":";
              int result = m_score;
              out << result << Qt::endl;
      }
       file.close();
}

Name::~Name()
{
//    delete ui;
}
